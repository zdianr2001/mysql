<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Array</title>
</head>
<body>
    <h1>Berlatih Array</h1>
        
    <?php 
        echo "<h3> Soal 1 </h3>";
        /* 
            SOAL NO 1
            Kelompokkan nama-nama di bawah ini ke dalam Array.
            Kids : "Mike", "Dustin", "Will", "Lucas", "Max", "Eleven" 
            Adults: "Hopper", "Nancy",  "Joyce", "Jonathan", "Murray"
        */
        $kids = ['Mike, ', 'Dustin, ', 'Will, ', 'Lucas, ', 'Max, ', 'Eleven'] ;
        echo "Kids: " . $kids[0] . $kids[1] . $kids[2] . $kids[3] . $kids[4] . $kids[5] .      "<br>" ; // Lengkapi di sini
        $adults = ['Hopper, ', 'Nancy, ', 'Joyce, ', 'Jonathan, ', 'Max, ', 'Murray'] ;
        echo "Adults: " .  $adults[0] . $adults[1] . $adults[2] . $adults[3] . $adults[4] . $adults[5] ;
        
        echo "<h3> Soal 2</h3>";
        /* 
            SOAL NO 2
            Tunjukkan panjang Array di Soal No 1 dan tampilkan isi dari masing-masing Array.
        */
        echo "Cast Stranger Things: ";
        echo "<br>";
        echo "Total Kids: " . count($kids) ; // Berapa panjang array kids
        echo "<br>";
        echo "<ol>"; 
        echo "<li> $kids[0] </li> <li> $kids[1] </li> <li> $kids[2] </li> <li> $kids[3] </li> <li> $kids[4] </li> <li> $kids[5] </li>" ;
        // Lanjutkan

        echo "</ol>";
        
        echo "Total Adults: " . count($adults) ; // Berapa panjang array adults
        echo "<br>"; 
        echo "<ol>";
        echo "<li> $adults[0] </li> <li> $adults[1] </li> <li> $adults[2] </li> <li> $adults[3] </li> <li> $adults[4] </li> <li> $adults[5] </li>";
        // Lanjutkan

        echo "</ol>";

        /*
            SOAL No 3
            Susun data-data berikut ke dalam bentuk Asosiatif Array (Array Multidimensi)
            
            Name: "Will Byers"
            Age: 12,
            Aliases: "Will the Wise"
            Status: "Alive"

            Name: "Mike Wheeler"
            Age: 12,
            Aliases: "Dungeon Master"
            Status: "Alive"

            Name: "Jim Hopper"
            Age: 43,
            Aliases: "Chief Hopper"
            Status: "Deceased"

            Name: "Eleven"
            Age: 12,
            Aliases: "El"
            Status: "Alive"
            
        */
        $data = array ( 
        array( "Name: Will Byers <br>" ,"Age:  12<br>" , "Aliases: Will the Wise<br>",
               "Status: Alive <br><br>") ,

        array( "Name: Mike Wheeler<br>" , "Age:  12 <br>", "Aliases: Dungeon Master <br>" ,
               "Status: Alive <br><br>" ) , 

        array( "Name: Jim Hopper <br>", "Age:  43 <br>", "Aliases: Chief Hopper <br>" ,
                "Status: Deceased <br><br>" ) ,

        array( "Name: Eleven  <br>", "Age:   12 <br>" , "Aliases: El <br>" , 
               "Status: Alive" )
    );
        echo "<h3> Soal 3 </h3>";
    echo $data [0][0] . $data[0][1] . $data[0][2] . $data[0][3]  ;
    echo $data [1][0] . $data[1][1] . $data[1][2] . $data[1][3]  ;
    echo $data [2][0] . $data[2][1] . $data[2][2] . $data[2][3]  ;
    echo $data [3][0] . $data[3][1] . $data[3][2] . $data[3][3]  ;

       

    ?>
</body>
</html>